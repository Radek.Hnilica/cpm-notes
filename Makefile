# ! make
# $Id: Makefile,v 1.4 2023/09/14 08:04:21 radekhnilica Exp $
# $Source: /Users/radekhnilica/st/repo/cpm/src/cpm-notes/Makefile,v $
# Makefile for transforming org sources to output formats especially pdf.
# Copyright (c) 2023 Radek Hnilica

# $Log: Makefile,v $
# Revision 1.4  2023/09/14 08:04:21  radekhnilica
# Generate also single page HTML.
#
# Revision 1.3  2023/08/30 20:47:36  radekhnilica
# Reuse Makefile from another project.
#
# Revision 1.4  2023-08-04 20:41:14+02  radek
# Change syntax highlighting scheme to monochrome, just to see.
#
# Revision 1.3  2023/07/06 18:48:38  radekhnilica
# Change depth of generating TOC
#
# Revision 1.2  2023/07/06 12:43:52  radek
# The project was moved to agonl2 repo, some modifications were needed.
# Fetch back generated pdf do it can be in git.
#

orgs	= $(wildcard *.org)	# Documents are recognised by '.org'
mds	= $(wildcard *.md)	# Markdown documents recognised by '.md'
pdfs	= $(orgs:.org=.pdf)	$(mds:.md=.pdf)
htmls	= $(orgs:.org=.html)	#$(mds:.md=.html)
txts	= $(orgs:.org=.txt)	$(mds:.md=.txt)
outputs = $(addprefix $(output)/,$(pdfs)) $(addprefix $(output)/,$(txts)) $(addprefix $(output)/,$(htmls))

# Directory for created pdf, html and texts.
output	= $(HOME)/st/note/out
VPATH	= $(output)

# Get the system os we are running on
SYSTEM=$(shell uname -s)

# Other pandoc options to be considered:
# --preserve-tabs
# --highlight-style tango :: this stile also made light shadow background
# other styles are: pygments, kate, monochrome, espresso, zenburn, haddock, and textmate tango
PANDOC_OPTS= --tab-stop=8
PANDOC_PDF_OPTS = $(PANDOC_OPTS) -V geometry:a4paper,margin=3cm
PANDOC_PDF_OPTS += --number-sections --toc --toc-depth=4
ifeq ($(SYSTEM), Darwin)
PANDOC_PDF_OPTS += --pdf-engine xelatex --highlight-style monochrome
else
PANDOC_PDF_OPTS += --highlight-style tango
endif
PANDOC_PDF_OPTS +=  $(COLOR_LINKS)
PANDOC_TXT_OPTS  = $(PANDOC_OPTS) -t plain
PANDOC_HTML_OPTS = $(PANDOC_OPTS) -t html -N
COLOR_LINKS = -V colorlinks=true -V linkcolor=blue -V urlcolor=red -V toccolor=gray

# PANDOC_PDF_OPTS	= $(PANDOC_OPTS) -V geometry:a4paper,margin=2cm --number-sections --toc --highlight-style tango $(COLOR_LINKS)
# PANDOC_TXT_OPTS = $(PANDOC_OPTS)
# COLOR_LINKS = -V colorlinks=true -V linkcolor=blue -V urlcolor=red -V toccolor=gray

default: all

all:	$(outputs)

clean:
	rm -v *~ # $(pdfs) $(txts)

### Rules

$(output)/%.txt: %.org
	pandoc $(PANDOC_TXT_OPTS) -o $@ $<

$(output)/%.pdf: %.org
	pandoc $(PANDOC_PDF_OPTS) -o $@ $<
	cp $@ .

$(output)/%.html: %.org
	pandoc $(PANDOC_HTML_OPTS) -o $@ $<
	cp $@ .

$(output)/%.txt: %.md
	pandoc $(PANDOC_TXT_OPTS) -o $@ $<

$(output)/%.pdf: %.md
	pandoc $(PANDOC_PDF_OPTS) -o $@ $<

#Eof:$Id: Makefile,v 1.4 2023/09/14 08:04:21 radekhnilica Exp $
